Files:	           Description:
----------------------------------------------------------------
upload_bot         - Automates uploading files.

download_bot       - Automates downloading files.

cmd_bot            - Automates sending commands.

remote             - Is an interface for the above automation.

cluster_machines   - list of cluster machines that you want to 
                     ssh or scp.
