#!/usr/bin/ruby -w
# This script helps create an automatic login with SSH
# without a password. By creating a rsa key pair.
# By Chris Choi
require 'rubygems'
require 'net/ssh'
require 'net/scp'

pinged_machines = "./pinged_machines"

# Check SSH connection first on the remote machines!!
if system("bash ./test_connection.sh") then
# The program nmap displays a lot of rubbish 

	# Program interface
	puts "This script helps create an automatic login with SSH"
	puts "so that you don't have to SSH with a password in the future"
	puts "First I need your login for the machines you wish to gain access to:"
	puts "Login:"
	user = gets.chomp
	puts "Password:"
	pass = gets.chomp
	status = 0

	# Login to the remote machines and create the ~/.ssh folder
	# and upload the rsa key pair into the folder.
	fp = File.open(pinged_machines)
	fp.each do |machine|
		if machine.include?("#") then
			puts "Skipping #{machine}"	
		else
			# Creating ~/.ssh folder on the remote machines
			puts "Creating .ssh folder on #{machine}"
			Net::SSH.start( machine.chomp, user, :password => pass ) do |session|
				error = session.exec!("mkdir -p ~/.ssh")
				status = session.exec!("echo $?")
				status = status.chomp
			end
		end
	end
	fp.close

	if status.chomp == '0' then 
		# Create the rsa key pair
		system('ssh-keygen -t rsa')
		puts ""
		puts ""
		system('bash upload_rsa.sh')
		puts "Done! Now SSH into the machines and try if you can login without entering"
		puts "a password! By doing: ssh some_user_name@some_host"

	else
		puts "Opps! There seems to be some problems with #{machine}"
		puts "Error: #{error}"
	end

	# Warning Note!
	puts ""
	puts "IMPORTANT NOTE!!:"
	puts "If this small script does not work, depending on your version of SSH" 
	puts "you might have to do the following"
	puts "- Put the public key in '.ssh/auhtorized_keys2'"
	puts "- Change the permission of .ssh to 700"
	puts "- Change the permission of .ssh/authorized_keys2 to 640"
end
