#!/bin/bash

machine_list="./machine_list"
pinged_machines="./pinged_machines"

# check for the program nmap
# exit with error if does not exist
nmap -d0 > /dev/null ; errors=$?
if [ "$errors" -ne "0" ]; then
	echo "Can't find nmap";
	echo "Cannot continue to test connection, exiting with errors!";
	echo "*************************************************************";
	echo "* Please install nmap inorder for the program to be able to *";
	echo "* check the ssh connections to the machines                 *";
	echo "*************************************************************";
	exit -1;
else
	echo "Found nmap! Now proceeding to check connections";
fi

# Erase the previous machine_list_checked if it exists
echo "Checking connection with machines"
if [ -f $pinged_machines ]; then
	rm $pinged_machines;
fi

# for every machine in the file machine_list
# check the connection
for machine in $(cat $machine_list); do
	echo "";
	echo "Trying out $machine";

	# testing connection with nmap
	nmap -PN $machine -p ssh > temp;
	# parsing the output of nmap
	cat temp | grep open ; status=$?;
	
	# if the word "open" exists in the temp file
	# it means that the ssh connection is open
	# if connection is open, put it in the pinged_machines file
	if [ "$status" -eq "0" ]; then
	{
		echo "$machine is OK!";
		echo "$machine" >> $pinged_machines;
	}
	else
	{
		echo "No available SSH connection from $machine!";
	}
	fi
done
