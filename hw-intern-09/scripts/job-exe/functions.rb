#!/usr/bin/ruby -w
# This file contains functions to perform the 
# functionalities of trivial paralllization
# By Chris Choi

require 'rubygems'
require 'fileutils'
# The following library are REQUIRED! 
# Read the README file for more info.
require 'net/ssh'
require 'net/scp'

class TrivParaSession

	# function downloads files from the cluster machines to a 
	# designated location on the local machine
	def download (addr, login, passwd, from)
		check_status = 0
		download = 0

		# Checking if the location exists
		while check_status == 0
			puts "Where do you want to save the files  to?"
			puts "You are currently in #{Dir.getwd}"
			puts ""
			printf("path: ")
			save_path = gets.chomp
			data_file_path = from + "/data/."
			
			if File.directory?(save_path) then
				# You are set to donwload
				download = 1
				check_status = 1
			else 
				puts "Error!: #{save_path} does not exist!"
				puts "Try again?(y/n)"
				ans = gets.chomp
				if ans == "y" || ans == "Y" then
					check_status = 0		
				else
					puts "Error!: Could not find the save path: #{save_path}"
					check_status = 1
				end
			end
			if download == 1 then
				Net::SCP.start( addr, login, :password => passwd ) do |scp|
					channel = scp.download( data_file_path , save_path, :recursive => true )
					puts "Downloading ..."
					channel.wait
					puts "Download Complete!"
				end
			end
		end
		
	end

	# uploads files from a user specified path, to a default remote location on
	# cluster machines
	def upload (addr, login, passwd, to)

		check_status = 0
		upload = 0
		while check_status == 0
			# ask for the file to upload
			puts "Where is the file you wish to upload?"
			puts "****************************************************"
			puts "*    REMEMBER TO PUT THE FULL OR RELATIVE PATH     *"
			puts "****************************************************"
			puts "You are currently in #{Dir.getwd}"
			puts "Here are the contents of the dir:"
			puts ""
			Dir.glob("*") do |f| puts f	end
			printf("\npath: ")
			local_path = gets.chomp

			# check if the file exists
			if File.exists?(local_path) then
				# you are set to upload
				upload = 1
				check_status = 1
			else
				# if the file doesn't exist ask again 
				puts "Error!: #{local_path} does not exist!"
				puts "Try again?(y/n)"
				ans = gets.chomp
				if ans == "y" || ans == "Y" then
					check_status = 0		
				else
					puts "Error!: Could not find the file: #{local_path}"
					check_status = 1
				end
			end
			
			# if file is found, we are ready to upload
			if upload == 1 then
				Net::SCP.start( addr, login, :password => passwd) do |scp|
					channel = scp.upload( local_path, to)
					puts "Uploading ..."
					channel.wait
					puts "Upload Complete"
				end
			end
		end

	end


	def exejob( addr, login, passwd, path_of_exe)
		puts "Have you uploaded the job file?(y/n)"
		ans = gets.chomp
		
		check_status = 0
		done = 0
		execute = 0

		while done == 0 
			if ans == "y" || ans == "Y" then
				puts "OK! starting the job"
				# ssh into the gateway and start the "executer!"
				Net::SSH.start( addr, login, :password => passwd) do |ssh|
					# the following passes a bash command to start the executer,
					# it takes argumenst of username, password, and job file
					ssh.exec("ruby #{path_of_exe} #{login} #{passwd} job.xml")
					puts "Done!"
				end
				done = 1
			else
				puts "Where is the job file you wish to start processing?"
				puts "You are currently in #{Dir.getwd}"
				puts "Here are the contents of the dir:"
				printf("\npath: ")
				jobfile_path = gets.chomp

				# Checking if the file exists
				while check_status == 0
					if File.exists?(jobfile_path) then
						# You are set to upload
						upload = 1
					else 
						puts "Error!: #{jobfile_path} does not exist!"
						puts "Try again?(y/n)"
						ans = gets.chomp
						if ans == "y" || ans == "Y" then
							check_status = 0		
						else
							puts "Error!: Could not find the job file: #{jobfile_path}"
							check_status = 1
						end
					end
				end # end of 2nd while loop (check if jobfile exists on local)
			end # end of if else in 1st while loop 
		end # end of 1st while loop (check if exejob is done)
	end

end
