#!/usr/bin/ruby -w
# This is a better version of the previous script I wrote to 
# access multiple linux machines in parallel. In this version
# the user has the ability to perform downloads, uploads and 
# execute most normal shell commands in parallel.
# By Chris Choi

require 'functions.rb'
# the following libraries are REQUIRED!
# read the README file for more info
require 'net/ssh'

# SENSITIVE INFO:
ssh_gateway='alder.eps.hw.ac.uk'
remote_save_path='~/TrivParaExe'
local_save_path='~/'


# User info
printf("login: ")
user = gets.chomp
printf("password: ")
passwd = gets.chomp

# attemp to login with the username and password provided by the user
Net::SSH.start( ssh_gateway, user, :password => passwd ) do |test| 
	test.exec("echo 'OK!'") 
end

# initiate a new session
session = TrivParaSession.new

# Interactive prompt interface
loop = 1
while loop == 1	
	# Command prompt
	printf(">> ")
	command = gets.chomp

	# Help
	if command == "help" then
		puts "Usage:"
		puts " upload    - upload to server" 
		puts " download  - download from server" 
		puts " exejob    - to start executing jobs"  
		puts " help      - for available commands"
		puts ""
	# Quit
	elsif command == "quit" || command == "exit" then
		loop = 0

	# Upload
	elsif command =="upload" then
		session.upload( ssh_gateway, user, passwd, remote_save_path)

	# Download
	elsif command == "download" then
		session.download( ssh_gateway, user, passwd, remote_save_path)

	# Execute
	elsif command == "exejob" then
		session.exejob(	ssh_gateway, user, passwd, remote_save_path )
	
	# if the command is not recognised
	else
		puts "Incorrect or unknown command! Please try again!"
		puts "Or type 'help' for available commands"	
	end # End of the prompt options if/else loop
	
end # End of the while loop
