#!/usr/bin/ruby -w
# This is an executer script that is launched remotely.
# It reads the job.xml file and acts as a launcher to 
# execute different jobs remotely one by one.
#
# The script accepts commandline arguments
# job-exe "username" "password"

require 'rubygems'
require 'xmlsimple'
require 'net/ssh'
require 'rubyscript2exe'

# sensitive information
username = "sg188"
password = "8946567"

# paths to job and cluster xml files
job_file_path = "./job.xml"

# Parse job and cluster xml file with xmlsimple library
puts "Parsing the job xml file"
job_file = XmlSimple.xml_in( job_file_path, { 'KeyAttr' => 'name' }) 

# Now that we have the all the machine addresses
# loop through the job.xml for jobs to process and start them
machine = Array.new
command = Array.new
loop = 1
counter = 0

while loop == 1
	p job_file['server'][0]
	if job_file['server'][counter] then
		machine << job_file['server'][counter]
		counter += 1
	else
		loop = 0
	end
end	

machine.each do |host|
	# put all the commands in array ~ command
	job_file['server'][host]['command'].each do |x|
		command << x
	end

	# start the jobs
	Net::SSH.start( host, username, :password => password) do |ssh|
		# execute command to host
		command.each do |c|
			ssh.exec!(c)
			puts "Execution of command #{c} on #{host} has started!"
		end
	end
	puts "No more jobs for #{host}"
end
