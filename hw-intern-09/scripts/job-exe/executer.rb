#!/usr/bin/ruby -w
# This is an executer script that is launched remotely.
# It reads the job.xml file and acts as a launcher to 
# execute different jobs remotely one by one.
#
# The script accepts commandline arguments
# job-exe "username" "password"

require 'rubygems'
require 'xmlsimple'
require 'net/ssh'
require 'rubyscript2exe'

# sensitive information
username = "sg188"
password = "8946567"

# paths to job and cluster xml files
job_file_path = "./job.xml"
cluster_file_path = "./cluster.xml"

# the cluster name given in the cluster.xml file
name_of_cluster="eps"

# Parse job and cluster xml file with xmlsimple library
puts "Parsing the job xml file"
job_file = XmlSimple.xml_in( job_file_path, { 'KeyAttr' => 'name' }) 
puts "Parsing the cluster xml file"
cluster_file = XmlSimple.xml_in( cluster_file_path, { 'KeyAttr' => 'name' }) 

# loop through the cluster.xml file for the adress of the machines
loop = 1
counter = 0
machine = Array.new
while loop == 1
	# if the cluster file still has machine adresses then put them in the 
	# machine array.
	if cluster_file['cluster'][name_of_cluster]['machine'][counter] then
		machine << cluster_file['cluster'][name_of_cluster]['machine'][counter]
		counter += 1
	else	# else no more machines, stop the loop
		loop = 0	
	end
end

# Now that we have the all the machine addresses
# loop through the job.xml for jobs to process and start them
machine.each do |host|
	command = Array.new
	output_file = Array.new
	
	# if there is a job for the machine then
	if job_file['server'][host] then
		# put all the commands in array ~ command
		job_file['server'][host]['command'].each do |x|
			command << x
		end
		# start the jobs
		Net::SSH.start( host, username, :password => password) do |ssh|
			# execute command to host
			command.each do |c|
				ssh.exec!(c)
				puts "Execution of command #{c} on #{host} has started!"
			end
		end
	# else print no jobs for a particular machine
	else
		puts "No jobs for #{host}"
	end

end



