#!/usr/bin/ruby

class TagsAnalysis 

	# "count" method returns the number of "Total Tags" or "Single Tags"
	# in the data file
  def count (file)
    t_count = 0
    s_count = 0
    
    datafile = File.open(file)
    datafile.each do |line|
      key, value = line.split("\t")
      t_count += value.to_i
      s_count += 1 if value.to_i == 1
    end
    datafile.close
    return {:single_tags => s_count, :total_tags => t_count}
  end


 	# This method takes in the clustered data, normalizes the data 
	# and outputs it into another file, with "_normalized.txt" at the end 
	# of the file.
  def normalize (datafile, save_path)
    hash = Hash.new  
    t_count = 0
		
		# Open up the datafile to obtain the Total Tag Count
    file = File.open(datafile)
    file.each do |line|
      key, value = line.chomp.split("\t")
      t_count += value.to_i
    end
    file.close
    
		
		# Normalization Phase
		# The Formula for normalization is:
		# normalized result = tag read * 100 000 000 / totol tag count
		# Normalization Factor out side the loop, to save computer resources and time)
		normaliztion_factor = 1000000.0 / t_count.to_f
	
		# Open the data file for normalization
    file = File.open(datafile)
		# Create a new file for the normalized results
    n_file = File.new(save_path + "/" + File.basename(datafile, "_clustered.txt") + "_normalized.txt", "w")
    
		puts "Normalizing datafile: " + File.basename(datafile)
    
		normalization_factor = 100000000.0 / t_count.to_f
		file.each do |line|
      key, value = line.chomp.split("\t")
      # Normalization occurs here!
			n_value =  value.to_f * normalization_factor
			# Printing the normalized reads upto 20 decimal places
			n_file.printf("%s\t%.20f\n",key, value)
    end
    file.close
		n_file.close
	end


	# Prints a giant table of all the normalized data
	# The order of the tags are determined by the "order" file
	def print_table( normalized_data_path, table_save_path, order_file_path)
		tag_list=Hash.new
		hash = Hash.new { |h, key| h[key] = Hash.new }

		# Obtain the normalized data
		# 1 - Create a Tag's list so that all the tags are accounted for
		# 2 - Put all the tags and reads into the hash
		# 3 - Read through the tag_list and print out the reads from the hash
		Dir.glob(normalized_data_path + "/*") do |datafile|
			file_name = File.basename(datafile, ".txt")
			puts "Processing: #{datafile}"
			File.foreach(datafile) do |line|
				key, value = line.chomp.split("\t")
				# Put all tags/reads into tag_list
				tag_list[key]=value
				# Put all the tags/reads into hash
				hash[file_name][key]=value
			end
		end

		# Start the procedure of printing out the giant table
		# using the tag_list I built previously
		# The order of the tags are determined by the "order" file
		puts "Starting stage 2: Comparing the tags and printing them!"
		
		# Create a new file to save the giant table
		table = File.new( table_save_path + "/table.txt", "w")
		
		# Go through the tag_list and print out the reads
		tag_list.each do |key, value|
			tag_value = 0
			# Get the order of which the table is to be displayed
			order_fp = File.open(order_file_path)

			# Print the tag
			table.printf("%s\t", key)
			# Print the read, sequence determined by the order file
			order_fp.each do |file|
				file_name = File.basename(file.chomp, ".txt")
				# From the hash, if the tag cannot be found in the specific datafile
				# the read is automatically 0
				if hash[file_name].has_key?(key) then
					tag_value = hash[file_name][key]
				end
				# Print the read
				table.printf("%.2f\t\t", tag_value)
			end
			order_fp.close
			# Add new line to table
			table.printf("\n")
		end
		table.close
	end
	
end


