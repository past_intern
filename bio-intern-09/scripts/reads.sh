#!/bin/bash
# Gets the number of total/single reads from clustered tags files
# Coded by Chris Choi

if [ "$1" = "-total" ]; then
	if [ -f "$2" ]; then
		echo "OK! Reading $2 ..."
		result=0
		for i in $(awk '{ print $2 }' $2); do
			let result=$result+$i	
		done
		echo "The total read is: $result"
	else
		echo "Can't find the file!"
	fi
elif [ "$1" = "-single" ]; then
	if [ -f "$2" ]; then
		echo "OK! Reading $2 ..."
		counter=0
		for i in $(awk '{ print $2 }' $2); do
			if [ "$i" = "1" ]; then
				let counter=$counter+1	
			fi
		done
		echo "The number of single reads are: $counter"
	else
		echo "Can't find the file!"
	fi
else
	echo "Usage: reads [options] [file]"
	echo ""	
	echo "Options:"
	echo "-total		gets the \"Total Reads\" in the data file"
	echo "-single		gets the number of \"Single Reads\" from the data file"
	echo ""
	echo "File:"
	echo "Path of the file you wish to get the total reads of."
	echo ""
fi




