#!/usr/bin/perl 
# Usage: compare file1.txt file2.txt
use strict;
my($argcnt) = $#ARGV + 1;
my %file1;
my %file2;

	if( $argcnt != 2){
		print "Usage: common_tags file1 file2\n";
		exit 1;
	}
	
	open(FILE1, $ARGV[0]) || die "Cannot open $ARGV[0] : $!\n";
	open(FILE2, $ARGV[1]) || die "Cannot open $ARGV[1] : $!\n";

	while( my $rec = <FILE1> ){
	    my($key,$value) = split /\s+/, $rec;
	    $file1{$key} = $value;
	}

	while( my $rec = <FILE2> ){
	    my($key,$value) = split /\s+/, $rec;
	    $file2{$key} = $value;
	}

	foreach my $key ( sort keys %file1 ){
	    #   Print intersection of files...
	    if( exists $file2{ $key } ){
	        print "$key\t ",$file1{$key}, "\t", $file2{$key},"\n";
	    }
	}
	close FILE2; 
	close FILE1;
	exit 0;
