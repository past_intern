#!/usr/bin/ruby	
require 'analysis.rb'

clustered_data_path = "../data/clustered_data"
normalized_data_path = "../data/normalized_data"
order_file_path = "./order" # Determines the order of the giant table
table_save_path = "../data/table" # Print out of all the normalized data 

analyze = TagsAnalysis.new

### Normalizing the clustered data and save it ###
Dir.glob(clustered_data_path + "/*") do |file|
	analyze.normalize(file, normalized_data_path)
end

### Printing out the giant table the clever way! (I hope) ###
analyze.print_table(normalized_data_path, table_save_path, order_file_path)














