All the data and scripts used to analyze the data of Dr Thomas Waibels PhD project.

File:                     Description:
archive                   Backup of all the data/script files
archive/data              Backup of data files
archive/data/depreciated  Data no longer useful
archive/scripts           Backup of script files

R-packages                Packages used for analysis of data

data                      Data used for bioinformatics analysis
data/clustered_tags       Clustered tags data
data/common_tags          Common tags data
data/normalized_tags      Normalized tags data

scripts                   Scripts used
scripts/reads             Returns the number of total/single reads from clustered tags data files
scripts/common_tags       Compares and outputs the common sequence of two sample data files 
scripts/analysis.rb       Ruby class for all the analysis involved for this project
scripts/main.rb           Main ruby script for analyzing the data 
